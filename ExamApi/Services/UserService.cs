﻿using Dapper;
using ExamApi.Context;
using ExamApi.Interfaces;

namespace ExamApi.Services
{
    public class UserService : IUserService
    {
        public readonly DapperContext _dapperContext;

        public UserService(DapperContext dapperContext) 
        { 
            _dapperContext = dapperContext;
        }

        public async Task<List<DbModels.User>> GetUsers()
        {
            try
            {
                string sql = "SELECT * FROM [Exam].[dbo].[User]";

                using (var connection = _dapperContext.CreateConnectionDb()) 
                {
                    var result = await connection.QueryAsync<DbModels.User>(sql);

                    return result.ToList();
                }
            }
            catch (Exception ex) 
            {
                return new List<DbModels.User>();
            }
        }
    }
}
