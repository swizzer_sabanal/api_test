﻿using System.Data;
using System.Data.SqlClient;

namespace ExamApi.Context
{
    public class DapperContext
    {
        public readonly IConfiguration _configuration;

        public readonly string _connectString;

        public DapperContext(IConfiguration configuration)
        {
            _configuration = configuration;
            _connectString = _configuration.GetConnectionString("DataContext");
        }

        public IDbConnection CreateConnectionDb()
            => new SqlConnection(_connectString);
    }
}
