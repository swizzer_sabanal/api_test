﻿using ExamApi.DbModels;

namespace ExamApi.Interfaces
{
    public interface IUserService
    {
        Task<List<User>> GetUsers();
    }
}
